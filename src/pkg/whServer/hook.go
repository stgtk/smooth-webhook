package whServer

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/stgtk/smooth-webhook/src/pkg/config"
	"gitlab.com/stgtk/smooth-webhook/src/pkg/runner"
	"gopkg.in/go-playground/webhooks.v5/gitlab"
	"net/http"
)

type ServerHook struct {
	Config  config.HookConfig
	Webhook *gitlab.Webhook
}

func (h ServerHook) PushWebhook(w http.ResponseWriter, r *http.Request) {
	payload, err := h.Webhook.Parse(r, gitlab.PushEvents)
	if err != nil {
		if err == gitlab.ErrEventNotFound {
			logrus.Error("I was not asked to parse this event: ", err)
		} else {
			logrus.Error("Error while parsing request: ", err)
		}
	}

	switch payload.(type) {
	case gitlab.PushEventPayload:
		pushRequest := payload.(gitlab.PushEventPayload)
		ru := runner.NewRunner(h.Config)
		go ru.PushRunner(pushRequest)
	}
}
