package whServer

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/stgtk/smooth-webhook/src/pkg/config"
	"gopkg.in/go-playground/webhooks.v5/gitlab"
	"net/http"
)

type Server struct {
	Config config.Config
	Hooks  []ServerHook
}

func NewServer(config config.Config) Server {
	server := Server{}
	server.Config = config
	for _, hook := range server.Config.Hooks {
		server.addHook(hook)
	}
	return server
}

func (s *Server) addHook(hookConfig config.HookConfig) {
	webhook, err := gitlab.New(gitlab.Options.Secret(hookConfig.Secret))
	if err != nil {
		logrus.Error(err)
	}
	s.Hooks = append(s.Hooks, ServerHook{
		Config:  hookConfig,
		Webhook: webhook,
	})
}

func (s *Server) Run() {
	s.addHandleFunctions()
	err := http.ListenAndServe(fmt.Sprintf(":%d", s.Config.Port), nil)
	if err != nil {
		logrus.Error(err)
	} else {
		logrus.Info("Server runs at port ", s.Config.Port)
	}
}

func (s *Server) addHandleFunctions() {
	for _, hook := range s.Hooks {
		http.HandleFunc(hook.Config.Endpoint, hook.PushWebhook)
	}
}
