package runner

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/solutionsbuero/gosmooth"
	"gitlab.com/stgtk/smooth-webhook/src/pkg/config"
	"gitlab.com/stgtk/smooth-webhook/src/pkg/file-stuff"
	"gitlab.com/stgtk/smooth-webhook/src/pkg/git"
	"gopkg.in/go-playground/webhooks.v5/gitlab"
	"path"
)

type Runner struct {
	HookConfig config.HookConfig
	FileStuff  file_stuff.FileStuff
}

func NewRunner(hookConfig config.HookConfig) Runner {
	return Runner{
		HookConfig: hookConfig,
		FileStuff:  file_stuff.NewFileStuff(hookConfig.OutputFolder),
	}
}

func (r Runner) PushRunner(payload gitlab.PushEventPayload) {
	r.FileStuff.InitFolders(r.HookConfig.Runs)

	gitService := git.NewGit(r.FileStuff.BasePath, payload.Project.GitSSSHURL)
	gitService.InitGitRepo()

	r.smoothHook(gitService.RepoDir)
}

func (r Runner) smoothHook(repoPath string) {
	for _, run := range r.HookConfig.Runs {
		r.doSmooth(path.Join(repoPath, run.InputFile), run.Name)
	}
}

func (r Runner) doSmooth(inputFile, endName string) {
	endName = file_stuff.NormalizeNames(endName)
	endName = endName + ".pdf"
	destination := path.Join(r.FileStuff.BasePath, endName)

	logrus.Debug("About to start gosmooth")
	smooth := gosmooth.NewSmooth(inputFile, path.Dir(inputFile))
	smooth.Run(destination)
	logrus.Debug("Gosmooth finished")
}
