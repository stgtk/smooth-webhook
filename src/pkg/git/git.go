package git

import (
	"fmt"
	"github.com/sirupsen/logrus"
	file_stuff "gitlab.com/stgtk/smooth-webhook/src/pkg/file-stuff"
	"os"
	"os/exec"
	"path"
	"strings"
)

type Git struct {
	WorkDir     string
	RepoDir     string
	RepoName    string
	SslUrl      string
	NewlyCloned bool
}

func NewGit(workDir, repoUrl string) Git {
	pathParts := strings.Split(repoUrl, "/")
	name := strings.Split(pathParts[len(pathParts)-1], ".")[0]
	return Git{
		WorkDir:  workDir,
		RepoDir:  path.Join(workDir, name),
		RepoName: name,
		SslUrl:   repoUrl,
	}
}

func (g Git) InitGitRepo() {
	if !file_stuff.Exists(g.RepoDir) {
		g.Clone()
	} else {
		g.Pull()
	}
}

func (g *Git) Clone() {
	logrus.Debug("About to git clone")
	err := os.Chdir(g.WorkDir)
	if err != nil {
		logrus.Error(err)
	}

	cmd := exec.Command("git", "clone", g.SslUrl)
	out, err := cmd.Output()
	if err != nil {
		logrus.Error("Error while git clone: ", err)
		fmt.Println(out)
	} else {
		logrus.Debug("Clone successful")
	}
	g.NewlyCloned = true
}

func (g Git) Pull() {
	if g.NewlyCloned {
		return
	}
	logrus.Debug("About to git pull")
	err := os.Chdir(g.RepoDir)
	if err != nil {
		logrus.Error(err)
	}

	cmd := exec.Command("git", "pull")
	err = cmd.Run()
	if err != nil {
		logrus.Error("Error while git pull: ", err)
	} else {
		logrus.Debug("Pull successful")
	}

}
