package file_stuff

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/stgtk/smooth-webhook/src/pkg/config"
	"os"
	"path"
	"strings"
)

type FileStuff struct {
	BasePath string
}

func NewFileStuff(basePath string) FileStuff {
	return FileStuff{
		BasePath: basePath,
	}
}

func (f FileStuff) InitFolders(runs []config.HookRun) {
	f.initBaseFolder()
	f.initRepoFolders(runs)
}

func (f FileStuff) initBaseFolder() {
	if !Exists(f.BasePath) {
		createFolder(f.BasePath)
	}
}

func (f FileStuff) initRepoFolders(runs []config.HookRun) {
	for _, run := range runs {
		documentDir := path.Join(f.BasePath, NormalizeNames(run.Name))
		if !Exists(documentDir) {
			createFolder(documentDir)
		}
	}
}

// System and internal functions

func Exists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	return false
}

func createFolder(path string) {
	err := os.MkdirAll(path, os.ModePerm)
	if err != nil {
		logrus.Error("Error while creation of folder(s): ", err)
	}
}

func NormalizeNames(name string) string {
	name = strings.Replace(name, " ", "-", -1)
	name = strings.Replace(name, "_", "-", -1)
	name = strings.Replace(name, ".", "-", -1)
	return strings.ToLower(name)
}
