package config

import (
	"encoding/json"
	"github.com/creasty/defaults"
	"github.com/sirupsen/logrus"
	"io/ioutil"
)

type Config struct {
	Port  int          `json:"port",default:"9090"`
	Hooks []HookConfig `json:"hooks",default:"[]"`
}

func NewConfig() *Config {
	config := Config{}
	if err := defaults.Set(&config); err != nil {
		logrus.Error("Default generation failed: ", err)
	}

	hook := &HookConfig{}
	if err := defaults.Set(hook); err != nil {
		logrus.Error("Default generation failed: ", err)
	}

	run := &HookRun{}
	if err := defaults.Set(run); err != nil {
		logrus.Error("Default generation failed: ", err)
	}
	hook.Runs = append(hook.Runs, *run)
	config.Hooks = append(config.Hooks, *hook)

	return &config
}

func OpenConfig(path string) Config {
	config := Config{}
	configJson, err := ioutil.ReadFile(path)
	if err != nil {
		logrus.Error("Error while reading from disk: ", err)
	}

	err = json.Unmarshal(configJson, &config)
	if err != nil {
		logrus.Fatal("Error while parsing JSON: ", err)
	}

	return config
}

func (c Config) SaveConfig(path string) {
	configJson, err := json.Marshal(c)
	if err != nil {
		logrus.Error("Error while JSON marshal: ", err)
	}
	err = ioutil.WriteFile(path, configJson, 0644)
	if err != nil {
		logrus.Error("Error while write JSON to disk: ", err)
	}
}

type HookConfig struct {
	Endpoint     string    `json:"endpoint",default:"playname"`
	Secret       string    `json:"secret",default:""`
	OutputFolder string    `json:"output_folder",default:"/var/smth-wbhk/playname"`
	Runs         []HookRun `json:"runs",default:"[]"`
}

type HookRun struct {
	Name      string `json:"name",default:"Name"`
	InputFile string `json:"input_file",default:"stageplay/main.md"`
}
