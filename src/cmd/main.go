package main

import (
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"gitlab.com/stgtk/smooth-webhook/src/pkg/config"
	"gitlab.com/stgtk/smooth-webhook/src/pkg/whServer"
	"os"
)

func main() {
	app := cli.NewApp()
	app.Name = "smooth-webhook"
	app.Usage = "GitLab webhook for gosmooth"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "config, c",
			Usage: "path to config file",
		},
	}
	app.Action = func(c *cli.Context) error {
		return runServer(c.Args().First())
	}
	app.Commands = []cli.Command{
		{
			Name:  "create-config",
			Usage: "create default config",
			Action: func(c *cli.Context) error {
				return newConfig(c.Args().First())
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		logrus.Fatal(err)
	}
}

func runServer(path string) error {
	logrus.SetLevel(logrus.DebugLevel)
	programConfig := config.OpenConfig(path)
	server := whServer.NewServer(programConfig)
	server.Run()
	return nil
}

func newConfig(path string) error {
	programConfig := config.NewConfig()
	programConfig.SaveConfig(path)
	return nil
}
