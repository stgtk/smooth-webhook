# Function and Usage

The tools relies on a config file (JSON). In this you can define an number of GitLab Repos and markdown files in them, which you want to convert. Smooth-Webhook listens for pushes. When a push happens, it clones or pulls the repo and builds the PDFs. For each repo, there is a root folder, so linking (for example Nextcloud External Storage) is easy. Further smooth-webhook listen to the tag `release` and produces a seperate file. So you can create shareable links, without people seeing your current drafts etc.

# Setup

## Requirements

- Ubuntu server or something alike
- The server has to be reachable from the internet
- The following packages installed:
    - `git`
    - `pandoc` (version >= 2.0)
    - `texlive`
    - `m4`
- Systemd
- GitLab configured with the public SSH Key

## Binary

Dowoad/build smooth-webhook binary

Copy binary to global location 

```
cp smooth-webhook /usr/local/bin/
```


## Dir and Config

Create a base dir for the program

```
mkdir /mnt/external_storage/smooth-webhook/
```

Create and open new config file with the following command:

```
smooth-webhook create-config /mnt/external_storage/smooth-webhook/config.json 
vim /mnt/external_storage/smooth-webhook/config.json
``` 

Change the config to your needs:

```
{
  "port": 6060,
  "hooks": [
    {
      "endpoint": "/myproject",
      "secret": "webhook-secret",
      "output_folder": "/mnt/external_storage/smooth-webhook/myproject",
      "runs": [
        {
          "name": "My Document",
          "input_file": "documentation/doc.md"
        }
      ]
    }
  ]
}
```


## User and Ownership

Create a new and group user

```
sudo useradd -M smooth-webhook-usr 
sudo usermod -L smooth-webhook-usr
sudo groupadd smooth-webhook
sudo usermod -a -G smooth-webhook smooth-webhook-usr
sudo mkdir /home/smooth-webhook-usr
```

Change ownerships

```
sudo chown -R smooth-webhook-usr:smooth-webhook /mnt/external_storage/smooth-webhook/
sudo chown -R smooth-webhook-usr:smooth-webhook /home/smooth-webhook-usr
```

## SSH Key

Create the SSH Keys and copy the public key to GitLab.

```
sudo -i -u smooth-webhook-usr
ssh-keygen -o -t rsa -b 4096 -C "email@example.com"
ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts
exit
```

## Service

```
sudo vim /etc/systemd/system/smooth-webhook.service
```
necessary
Insert the following content and modify it if necessary:


```
[Unit]
Description=smooth-webhook
After=syslog.target
After=network.target

[Service]
RestartSec=2s
Type=simple
User=smooth-webhook-usr
Group=smooth-webhook-usr
WorkingDirectory=/mnt/external_storage/smooth-webhook/
ExecStart=/usr/local/bin/smooth-webhook /mnt/external_storage/smooth-webhook/config.json
Restart=always

PermissionsStartOnly=true
ExecStartPre=/bin/mkdir -p /var/log/smooth-webhook
ExecStartPre=/bin/chown syslog:adm /var/log/smooth-webhook
ExecStartPre=/bin/chmod 755 /var/log/smooth-webhook
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=smooth-webhook

[Install]
WantedBy=multi-user.target
```

Enable and start service

```
sudo systemctl enable smooth-webhook
sudo systemctl start smooth-webhook
```

To see the log:

```
sudo journalctl -f -u smooth-webhook
```
